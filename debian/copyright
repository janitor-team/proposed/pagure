Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pagure
Source: https://pagure.io/pagure
Files-Excluded:
 pagure/static/vendor/bootstrap/*
 pagure/static/vendor/cal-heatmap/*.min.js
 pagure/static/vendor/codemirror/*
 pagure/static/vendor/d3/*
 pagure/static/vendor/dragula/*.min.js
 pagure/static/vendor/diff2html/*.min.js
 pagure/static/vendor/diff2html/*.min.css
 pagure/static/vendor/emojione/*.min.js
 pagure/static/vendor/emojione/*.min*.css
 pagure/static/vendor/font-awesome/*
 pagure/static/vendor/highlight.js/*
 pagure/static/vendor/highlightjs-line-numbers/*
 pagure/static/vendor/jquery/*
 pagure/static/vendor/jquery.atwho/*.min.js
 pagure/static/vendor/jquery.atwho/*.min.css
 pagure/static/vendor/jquery.caret/*
 pagure/static/vendor/jquery.textcomplete/*
 pagure/static/vendor/jstimezonedetect/*
 pagure/static/vendor/lazyload/*
 pagure/static/vendor/selectize/*
 pagure/hooks/files/default_hook.py
 pagure/hooks/files/git_multimail.py
 pagure/hooks/files/mirror.py
 pagure/hooks/files/pagure_block_unsigned.py
 pagure/hooks/files/pagure_force_commit_hook.py
 pagure/hooks/files/pagure_hook.py
 pagure/hooks/files/pagure_hook_requests.py
 pagure/hooks/files/pagure_hook_tickets.py
 pagure/hooks/files/pagure_no_new_branches
 pagure/hooks/files/rtd_hook.py
 pagure/themes/pagureio/*
 pagure/themes/srcfpo/*
 pagure/themes/chameleon/*
 doc/_build/*

Files: *
Copyright: 2014-2018 Red Hat, Inc
License: GPL-2+

Files: pagure/utils.py
Copyright: 2017 Red Hat, Inc
           2013-2014 Konsta Vesterinen
License: GPL-2+ and Expat

Files: pagure/hooks/files/git_multimail_upstream.py
Copyright: 2015-2016 Matthieu Moy and others
           2012-2014 Michael Haggerty and others
           2007 Andy Parkins
License: GPL-2

Files: pagure/static/toggle.css
Copyright: 2012-2013 Thibaut Courouble
License: Expat

Files: pagure/static/vendor/diff2html/diff2html.js
Copyright: 2009-2015, Kevin Decker <kpdecker@gmail.com>
           2011 Twitter, Inc.
           Joyent, Inc. and other Node contributors.
           2011 Andrei Mackenzie
License: BSD-3-Clause and Apache and Expat

Files: pagure/static/vendor/cal-heatmap/cal-heatmap.*
Copyright: 2013 Wan Qi Chen
License: Expat

Files: debian/*
Copyright: 2019 Sergio Durigan Junior <sergiodj@debian.org>
License: GPL-2+

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: BSD-3-Clause
 Copyright (c) 2009-2015, Kevin Decker <kpdecker@gmail.com>
 .
 All rights reserved.
 .
 Redistribution and use of this software in source and binary forms,
 with or without modification, are permitted provided that the
 following conditions are met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
  * Neither the name of Kevin Decker nor the names of its contributors
    may be used to endorse or promote products derived from this
    software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Apache
 Copyright 2011 Twitter, Inc.
 Licensed under the Apache License, Version 2.0 (the "License"); you
 may not use this file except in compliance with the License.  You may
 obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 implied.  See the License for the specific language governing
 permissions and limitations under the License.
